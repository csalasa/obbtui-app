import { Component } from '@angular/core';
import { LoadingController, NavController } from 'ionic-angular';
import { AccesoPage } from '../acceso/acceso';

@Component({
  selector: 'page-salir',
  template: ''
})
export class SalirPage {

  constructor(
    public nav: NavController,
    public loadingCtrl: LoadingController
  ) {
    let env = this;
    localStorage.clear();
    localStorage.setItem('presentacion', 'false');
    env.nav.setRoot(AccesoPage);
    env.nav.popToRoot();
  }
}
